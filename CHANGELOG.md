go-csp-evaluator changelog

# v1.0.2
- Add support wildcard as port number (!2) ref: https://github.com/google/csp-evaluator/pull/46/files
- Recognize the "navigate-to" nav directive (!2) ref: https://github.com/google/csp-evaluator/pull/44/files
- Add webrtc directive (!2) ref: https://github.com/google/csp-evaluator/pull/47/files 
- Add more tests (!2) ref: https://github.com/google/csp-evaluator/pull/50/files

# v1.0.1
- Fix incorrect constant in CheckRequiresTrustedTypesForScripts (!1)

# v1.0.0
- Initial release of go-csp-evaluator port from https://github.com/google/csp-evaluator
