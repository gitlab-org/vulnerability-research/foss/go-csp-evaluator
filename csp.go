package gocspevaluator

import (
	"strings"

	orderedmap "github.com/wk8/go-ordered-map"
)

type CSPVersion int

const (
	CSP1 CSPVersion = iota
	CSP2
	CSP3
)

type CSP struct {
	directives orderedmap.OrderedMap
}

func NewCSP() CSP {
	return CSP{directives: *orderedmap.New()}
}

func (c *CSP) String() string {
	cspValue := ""
	for pair := c.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		directiveValues := pair.Value.([]string)

		cspValue += directive.String()

		if directiveValues == nil {
			continue
		}

		for _, directiveValue := range directiveValues {
			cspValue += " "
			cspValue += directiveValue
		}
		cspValue += "; "
	}

	return cspValue
}

func (c *CSP) GetDirective(directive Directive) []string {
	values, _ := c.directives.Get(directive)
	if values == nil {
		return nil
	}
	elements := make([]string, 0)

	for _, v := range values.([]string) {
		elements = append(elements, v)
	}
	return elements
}

func (c *CSP) clone() CSP {
	csp := NewCSP()
	for pair := c.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		csp.directives.Set(directive, c.GetDirective(directive))
	}
	return csp
}

// EffectiveCSP returns CSP as it would be seen by a UA supporting a specific CSP version.
func (c *CSP) EffectiveCSP(version CSPVersion, optFindings ...Finding) CSP {
	findings := optFindings

	effectiveCSP := c.clone()
	directive := effectiveCSP.EffectiveDirective(SCRIPT_SRC)
	values := c.GetDirective(directive)
	effectiveCSPValues := effectiveCSP.GetDirective(directive)

	if len(effectiveCSPValues) > 0 && (effectiveCSP.PolicyHasScriptNonces() || effectiveCSP.PolicyHasScriptHashes()) {
		if version >= CSP2 {
			// Ignore 'unsafe-inline' in CSP >= v2, if a nonce or a hash is present.
			if Includes(values, KW_UNSAFE_INLINE.String()) {
				effectiveCSPValues = Remove(effectiveCSPValues, KW_UNSAFE_INLINE.String())
				findings = append(findings, Finding{
					FindingType: IGNORED,
					Description: `unsafe-inline is ignored if a nonce or a hash is present.	(CSP2 and above)`,
					Severity:  SEV_NONE,
					Directive: directive,
					Value:     KW_UNSAFE_INLINE.String(),
				})
			}
		} else {
			// remove nonces and hashes (not supported in CSP < v2).
			for _, value := range values {
				if strings.HasPrefix(value, `'nonce-`) || strings.HasPrefix(value, `'sha`) {
					effectiveCSPValues = Remove(effectiveCSPValues, value)
				}
			}
		}
	}

	if len(effectiveCSPValues) > 0 && c.PolicyHasStrictDynamic() {
		// Ignore allowlist in CSP >= v3 in presence of 'strict-dynamic'.
		if version >= CSP3 {
			for _, value := range values {
				// Because of 'strict-dynamic' all host-source and scheme-source
				// expressions, as well as the "'unsafe-inline'" and "'self'
				// keyword-sources will be ignored.
				// https://w3c.github.io/webappsec-csp/#strict-dynamic-usage
				if !strings.HasPrefix(value, `'`) || strings.EqualFold(value, KW_SELF.String()) ||
					strings.EqualFold(value, KW_UNSAFE_INLINE.String()) {

					effectiveCSPValues = Remove(effectiveCSPValues, value)
					findings = append(findings, Finding{
						FindingType: IGNORED,
						Description: `Because of strict-dynamic this entry is ignored in CSP3 and above`,
						Severity:    SEV_NONE,
						Directive:   directive,
						Value:       value,
					})
				}
			}
		} else {
			// strict-dynamic not supported.
			effectiveCSPValues = Remove(effectiveCSPValues, KW_STRICT_DYNAMIC.String())
		}
	}

	effectiveCSP.directives.Set(directive, effectiveCSPValues)

	if version < CSP3 {
		effectiveCSP.directives.Delete(REPORT_TO)
		effectiveCSP.directives.Delete(WORKER_SRC)
		effectiveCSP.directives.Delete(MANIFEST_SRC)
		effectiveCSP.directives.Delete(TRUSTED_TYPES)
		effectiveCSP.directives.Delete(REQUIRE_TRUSTED_TYPES_FOR)
	}
	return effectiveCSP
}

// EffectiveDirective returns default-src if directive is a fetch directive and is not present in
// this CSP. Otherwise the provided directive is returned.
func (c *CSP) EffectiveDirective(inDirective Directive) Directive {
	// Only fetch directives default to default-src.
	if !c.containsDirective(inDirective) && FetchDirectivesIncludes(Directive(inDirective)) {
		return DEFAULT_SRC
	}

	return inDirective
}

// EffectiveDirectives returns the passed directives if present in this CSP or default-src otherwise.
func (c *CSP) EffectiveDirectives(inDirectives []Directive) []Directive {

	effectiveDirectives := orderedmap.New()

	for _, directive := range inDirectives {
		effectiveDirectives.Set(c.EffectiveDirective(directive), struct{}{})
	}

	effective := make([]Directive, 0)

	for pair := effectiveDirectives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)

		effective = append(effective, directive)
	}

	return effective
}

func (c *CSP) PolicyHasScriptNonces() bool {
	directiveName := c.EffectiveDirective(SCRIPT_SRC)
	values := c.GetDirective(directiveName)
	for _, value := range values {
		if IsNonce(value, false) {
			return true
		}
	}
	return false
}

func (c *CSP) PolicyHasScriptHashes() bool {
	directiveName := c.EffectiveDirective(SCRIPT_SRC)
	values := c.GetDirective(directiveName)
	for _, value := range values {
		if IsHash(value, false) {
			return true
		}
	}
	return false
}

func (c *CSP) PolicyHasStrictDynamic() bool {
	directiveName := c.EffectiveDirective(SCRIPT_SRC)
	values := c.GetDirective(directiveName)
	for _, value := range values {
		if strings.Contains(value, KW_STRICT_DYNAMIC.String()) {
			return true
		}
	}
	return false
}

func (c *CSP) containsDirective(inDirective Directive) bool {
	for pair := c.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		if directive == inDirective {
			return true
		}
	}
	return false
}

func Includes(elements []string, key string) bool {
	for _, element := range elements {
		if strings.EqualFold(element, key) {
			return true
		}
	}
	return false
}

func Remove(elements []string, key string) []string {
	filtered := make([]string, 0)
	for _, x := range elements {
		if !strings.EqualFold(x, key) {
			filtered = append(filtered, x)
		}
	}
	return filtered
}
