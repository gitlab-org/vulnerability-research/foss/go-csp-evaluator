package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCSPEvaluator(t *testing.T) {
	fakeCSP := NewCSP()
	evaluator := NewCSPEvaluator(fakeCSP, CSP3)
	assert.Equal(t, fakeCSP, evaluator.csp)
}

func TestCSPEvaluatorEvaluate(t *testing.T) {
	fakeCSP := NewCSP()
	fakeFinding := Finding{
		FindingType: UNKNOWN_DIRECTIVE,
		Description: `Fake Description`,
		Severity:    SEV_MEDIUM,
		Directive:   "fake-directive",
		Value:       "fake-directive-value",
	}

	fakeFinderFn := func(parsedCSP CSP) []Finding {
		return []Finding{fakeFinding}
	}

	evaluator := NewCSPEvaluator(fakeCSP, CSP3)
	findings := evaluator.Evaluate([]CheckerFn{fakeFinderFn, fakeFinderFn}, []CheckerFn{fakeFinderFn})
	expected := []Finding{fakeFinding, fakeFinding, fakeFinding}

	assert.ElementsMatch(t, expected, findings)
}
