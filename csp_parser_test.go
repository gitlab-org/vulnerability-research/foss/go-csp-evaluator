package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type parserTest struct {
	directive string
	elements  []string
}

func TestCSPParser(t *testing.T) {
	validCSP := "default-src 'none';" +
		"script-src 'nonce-unsafefoobar' 'unsafe-eval'   'unsafe-inline' \n" +
		"https://example.com/foo.js foo.bar;      " +
		"object-src 'none';" +
		"img-src 'self' https: data: blob:;" +
		"style-src 'self' 'unsafe-inline' 'sha256-1DCfk1NYWuHMfoobarfoobar=';" +
		"font-src *;" +
		"child-src *.example.com:9090;" +
		"upgrade-insecure-requests;\n" +
		"report-uri /csp/test"

	parser := NewCSPParser(validCSP)

	parsedCSP := parser.Parse()

	// check directives
	keys := make([]Directive, 0)
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		keys = append(keys, pair.Key.(Directive))
	}

	expected := []Directive{
		"default-src", "script-src", "object-src", "img-src", "style-src",
		"font-src", "child-src", "upgrade-insecure-requests", "report-uri",
	}

	assert.ElementsMatch(t, expected, keys)

	// check directive values
	toTest := []parserTest{
		{
			"default-src",
			[]string{"'none'"},
		},
		{
			"script-src",
			[]string{"'nonce-unsafefoobar'", "'unsafe-eval'", "'unsafe-inline'", "https://example.com/foo.js", "foo.bar"},
		},
		{
			"object-src",
			[]string{"'none'"},
		},
		{
			"img-src",
			[]string{"'self'", "https:", "data:", "blob:"},
		},
		{
			"style-src",
			[]string{"'self'", "'unsafe-inline'", "'sha256-1DCfk1NYWuHMfoobarfoobar='"},
		},
		{
			"font-src",
			[]string{"*"},
		},
		{
			"child-src",
			[]string{"*.example.com:9090"},
		},
		{
			"upgrade-insecure-requests",
			[]string{},
		},
		{
			"report-uri",
			[]string{"/csp/test"},
		},
	}
	for _, test := range toTest {
		assert.ElementsMatch(t, test.elements, parsedCSP.GetDirective(Directive(test.directive)))
	}
}

func TestCSPParserDuplicateDirectives(t *testing.T) {
	validCSP := "default-src 'none';" +
		"default-src foo.bar;" +
		"object-src 'none';" +
		"OBJECT-src foo.bar;"

	parser := NewCSPParser(validCSP)

	parsedCSP := parser.Parse()

	// check directives
	keys := make([]Directive, 0)
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		keys = append(keys, pair.Key.(Directive))
	}

	expected := []Directive{"default-src", "object-src"}

	assert.ElementsMatch(t, expected, keys)

	// check directive values
	toTest := []parserTest{
		{
			"default-src",
			[]string{"'none'"},
		},
		{
			"object-src",
			[]string{"'none'"},
		},
	}
	for _, test := range toTest {
		assert.ElementsMatch(t, test.elements, parsedCSP.GetDirective(Directive(test.directive)))
	}
}

func TestCSPParserMixedCaseKeywords(t *testing.T) {
	validCSP := "DEFAULT-src 'NONE';" + // Keywords should be case insensetive.
		"img-src 'sElf' HTTPS: Example.com/CaseSensitive;"

	parser := NewCSPParser(validCSP)

	parsedCSP := parser.Parse()

	// check directives
	keys := make([]Directive, 0)
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		keys = append(keys, pair.Key.(Directive))
	}

	expected := []Directive{"default-src", "img-src"}

	assert.ElementsMatch(t, expected, keys)

	// check directive values
	toTest := []parserTest{
		{
			"default-src",
			[]string{"'none'"},
		},
		{
			"img-src",
			[]string{"'self'", "https:", "Example.com/CaseSensitive"},
		},
	}
	for _, test := range toTest {
		assert.ElementsMatch(t, test.elements, parsedCSP.GetDirective(Directive(test.directive)))
	}
}

func TestNormalizeDirectiveValue(t *testing.T) {
	assert.Equal(t, "'none'", NormalizeDirectiveValue("'nOnE'"))
	assert.Equal(t, "'nonce-aBcD'", NormalizeDirectiveValue("'nonce-aBcD'"))
	assert.Equal(t, "'hash-XyZ=='", NormalizeDirectiveValue("'hash-XyZ=='"))
	assert.Equal(t, "https:", NormalizeDirectiveValue("HTTPS:"))
	assert.Equal(t, "example.com/TEST", NormalizeDirectiveValue("example.com/TEST"))
}
