package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCSPConvertToString(t *testing.T) {

	testCSP := "default-src 'none'; " +
		"script-src 'nonce-unsafefoobar' 'unsafe-eval' 'unsafe-inline' " +
		"https://example.com/foo.js foo.bar; " +
		"img-src 'self' https: data: blob:; "

	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.Equal(t, testCSP, parsedCSP.String())
}

func TestEffectiveCSPVersion1(t *testing.T) {
	testCSP := "default-src 'unsafe-inline' 'strict-dynamic' 'nonce-123' " +
		"'sha256-foobar' 'self'; report-to foo.bar; worker-src *; manifest-src *"

	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	effectiveCSP := parsedCSP.EffectiveCSP(CSP1)

	assert.ElementsMatch(t, []string{"'unsafe-inline'", "'self'"}, effectiveCSP.GetDirective(DEFAULT_SRC))
	assert.Nil(t, effectiveCSP.GetDirective(REPORT_TO))
	assert.Nil(t, effectiveCSP.GetDirective(WORKER_SRC))
	assert.Nil(t, effectiveCSP.GetDirective(MANIFEST_SRC))
}

func TestEffectiveCSPVersion2(t *testing.T) {
	testCSP := "default-src 'unsafe-inline' 'strict-dynamic' 'nonce-123' " +
		"'sha256-foobar' 'self'; report-to foo.bar; worker-src *; manifest-src *"

	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	effectiveCSP := parsedCSP.EffectiveCSP(CSP2)

	assert.ElementsMatch(t, []string{"'nonce-123'", "'sha256-foobar'", "'self'"}, effectiveCSP.GetDirective(DEFAULT_SRC))
	assert.Nil(t, effectiveCSP.GetDirective(REPORT_TO))
	assert.Nil(t, effectiveCSP.GetDirective(WORKER_SRC))
	assert.Nil(t, effectiveCSP.GetDirective(MANIFEST_SRC))
}

func TestEffectiveCSPVersion3(t *testing.T) {
	testCSP := "default-src 'unsafe-inline' 'strict-dynamic' 'nonce-123' " +
		"'sha256-foobar' 'self'; report-to foo.bar; worker-src *; manifest-src *"

	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	effectiveCSP := parsedCSP.EffectiveCSP(CSP3)

	assert.ElementsMatch(t, []string{"'strict-dynamic'", "'nonce-123'", "'sha256-foobar'"}, effectiveCSP.GetDirective(DEFAULT_SRC))
	assert.ElementsMatch(t, []string{"foo.bar"}, effectiveCSP.GetDirective(REPORT_TO))
	assert.ElementsMatch(t, []string{"*"}, effectiveCSP.GetDirective(WORKER_SRC))
	assert.ElementsMatch(t, []string{"*"}, effectiveCSP.GetDirective(MANIFEST_SRC))
}

func TestEffectiveDirective(t *testing.T) {
	testCSP := "default-src https:; script-src foo.bar"

	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()
	script := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	assert.Equal(t, SCRIPT_SRC, script)

	style := parsedCSP.EffectiveDirective(STYLE_SRC)
	assert.Equal(t, DEFAULT_SRC, style)
}

func TestEffectiveDirectives(t *testing.T) {
	testCSP := "default-src https:; script-src foo.bar"

	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()
	directives := parsedCSP.EffectiveDirectives([]Directive{SCRIPT_SRC, STYLE_SRC})
	assert.ElementsMatch(t, []Directive{SCRIPT_SRC, DEFAULT_SRC}, directives)
}

func TestPolicyHasScriptNoncesScriptSrcWithNonce(t *testing.T) {
	testCSP := "default-src https: 'nonce-ignored'; script-src nonce-invalid"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.False(t, parsedCSP.PolicyHasScriptNonces())
}

func TestPolicyHasScriptHashesScriptSrcWithHash(t *testing.T) {
	testCSP := "default-src https:; script-src 'sha256-asdfASDF'"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.True(t, parsedCSP.PolicyHasScriptHashes())
}

func TestPolicyHasScriptHashesNoHash(t *testing.T) {
	testCSP := "default-src https: 'nonce-ignored'; script-src sha256-invalid"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.False(t, parsedCSP.PolicyHasScriptHashes())
}

func TestPolicyHasStrictDynamicScriptSrcWithStrictDynamic(t *testing.T) {
	testCSP := "default-src https:; script-src 'strict-dynamic'"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.True(t, parsedCSP.PolicyHasStrictDynamic())
}

func TestPolicyHasStrictDynamicDefaultSrcWithStrictDynamic(t *testing.T) {
	testCSP := "default-src https 'strict-dynamic'"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.True(t, parsedCSP.PolicyHasStrictDynamic())
}

func TestPolicyHasStrictDynamicNoStrictDynamic(t *testing.T) {
	testCSP := "default-src 'strict-dynamic'; script-src foo.bar'"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.False(t, parsedCSP.PolicyHasStrictDynamic())
}

func TestIsDirective(t *testing.T) {
	for d := range allDirectives {
		assert.True(t, IsDirective(d.String()))
	}
	assert.False(t, IsDirective("invalid-src"))
}

func TestIsKeyword(t *testing.T) {
	for kw := range allKeywords {
		assert.True(t, IsKeyword(kw.String()))
	}
	assert.False(t, IsKeyword("invalid"))
}

func TestIsURLScheme(t *testing.T) {
	assert.True(t, IsUrlScheme("http:"))
	assert.True(t, IsUrlScheme("https:"))
	assert.True(t, IsUrlScheme("data:"))
	assert.True(t, IsUrlScheme("blob:"))
	assert.True(t, IsUrlScheme("b+l.o-b:"))
	assert.True(t, IsUrlScheme("filesystem:"))
	assert.False(t, IsUrlScheme("invalid"))
	assert.False(t, IsUrlScheme("ht_tp:"))
}

func TestIsNonce(t *testing.T) {
	assert.True(t, IsNonce("'nonce-asdfASDF='", false))
	assert.False(t, IsNonce("'sha256-asdfASDF='", false))
	assert.False(t, IsNonce("'asdfASDF='", false))
	assert.False(t, IsNonce("example", false))
}

func TestIsStrictNonce(t *testing.T) {
	assert.True(t, IsNonce("'nonce-asdfASDF='", true))
	assert.True(t, IsNonce("'nonce-as+df/A0234SDF=='", true))
	assert.True(t, IsNonce("'nonce-as_dfASDF='", true))
	assert.False(t, IsNonce("'nonce-asdfASDF==='", true))
	assert.False(t, IsNonce("'sha256-asdfASDF='", true))
}

func TestIsHash(t *testing.T) {
	assert.True(t, IsHash("'sha256-asdfASDF='", false))
	assert.False(t, IsHash("'sha777-asdfASDF='", false))
	assert.False(t, IsHash("'asdfASDF='", false))
	assert.False(t, IsHash("example.com", false))
}

func TestIsStrictHash(t *testing.T) {
	assert.True(t, IsHash("'sha256-asdfASDF='", true))
	assert.True(t, IsHash("'sha256-as+d/f/ASD0+4F=='", true))
	assert.False(t, IsHash("'sha256-asdfASDF==='", true))
	assert.False(t, IsHash("'sha256-asd_fASDF='", true))
	assert.False(t, IsHash("'sha777-asdfASDF='", true))
	assert.False(t, IsHash("'asdfASDF='", true))
	assert.False(t, IsHash("example.com", true))
}

func TestParseNavigateTo(t *testing.T) {
	testCSP := "navigate-to 'self'; script-src 'nonce-foo'"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.False(t, parsedCSP.PolicyHasStrictDynamic())
	assert.True(t, parsedCSP.PolicyHasScriptNonces())
}

func TestParseWebRTC(t *testing.T) {
	testCSP := "web-rtc 'allow'; script-src 'nonce-foo'"
	parser := NewCSPParser(testCSP)
	parsedCSP := parser.Parse()

	assert.False(t, parsedCSP.PolicyHasStrictDynamic())
	assert.True(t, parsedCSP.PolicyHasScriptNonces())
}
