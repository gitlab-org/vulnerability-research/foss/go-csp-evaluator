package gocspevaluator

type Severity int

const (
	SEV_HIGH         Severity = 10
	SEV_SYNTAX       Severity = 20
	SEV_MEDIUM       Severity = 30
	SEV_HIGH_MAYBE   Severity = 40
	SEV_STRICT_CSP   Severity = 45
	SEV_MEDIUM_MAYBE Severity = 50
	SEV_INFO         Severity = 60
	SEV_NONE         Severity = 100
)

type FindingType int

const (
	// Parser checks
	MISSING_SEMICOLON FindingType = 100
	UNKNOWN_DIRECTIVE FindingType = 101
	INVALID_KEYWORD   FindingType = 102
	NONCE_CHARSET     FindingType = 106

	// Security cheks
	MISSING_DIRECTIVES      FindingType = 300
	SCRIPT_UNSAFE_INLINE    FindingType = 301
	SCRIPT_UNSAFE_EVAL      FindingType = 302
	PLAIN_URL_SCHEMES       FindingType = 303
	PLAIN_WILDCARD          FindingType = 304
	SCRIPT_ALLOWLIST_BYPASS FindingType = 305
	OBJECT_ALLOWLIST_BYPASS FindingType = 306
	NONCE_LENGTH            FindingType = 307
	IP_SOURCE               FindingType = 308
	DEPRECATED_DIRECTIVE    FindingType = 309
	SRC_HTTP                FindingType = 310

	// Strict dynamic and backward compatibility checks
	STRICT_DYNAMIC                FindingType = 400
	STRICT_DYNAMIC_NOT_STANDALONE FindingType = 401
	NONCE_HASH                    FindingType = 402
	UNSAFE_INLINE_FALLBACK        FindingType = 403
	ALLOWLIST_FALLBACK            FindingType = 404
	IGNORED                       FindingType = 405

	// Trusted Types checks
	REQUIRE_TRUSTED_TYPES_FOR_SCRIPTS FindingType = 500

	// Lighthouse checks
	REPORTING_DESTINATION_MISSING FindingType = 600
	REPORT_TO_ONLY                FindingType = 601
)

// A CSP Finding is returned by a CSP check and can either reference a directive
// value or a directive. If a directive value is referenced opt_index must be
// provided.
type Finding struct {
	FindingType FindingType
	Directive   Directive
	Description string
	Severity    Severity
	Value       string
}

// HighestSeverity returns the highest severity of a list of findings
func (f Finding) HighestSeverity(findings []Finding) Severity {
	if len(findings) == 0 {
		return SEV_NONE
	}

	highest := SEV_NONE
	for _, finding := range findings {
		if finding.Severity < highest {
			highest = finding.Severity
		}
	}

	return highest
}

func (f Finding) Equals(other Finding) bool {
	return f.FindingType == other.FindingType &&
		f.Directive == other.Directive &&
		f.Description == other.Description &&
		f.Severity == other.Severity &&
		f.Value == other.Value
}
