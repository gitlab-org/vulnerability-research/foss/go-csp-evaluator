package gocspevaluator

import (
	"fmt"
	"regexp"
	"strings"
)

/**
 * Checks if the csp contains invalid directives.
 *
 * Example policy where this check would trigger:
 *  foobar-src foo.bar
 *
 * @param parsedCsp A parsed csp.
 */
func CheckUnknownDirective(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)

		if IsDirective(directive.String()) {
			// directive is known
			continue
		}

		if strings.HasSuffix(directive.String(), ":") {
			findings = append(findings, Finding{
				FindingType: UNKNOWN_DIRECTIVE,
				Description: `CSP Directives don't end with a colon.`,
				Severity:    SEV_SYNTAX,
				Directive:   directive,
			})
		} else {
			findings = append(findings, Finding{
				FindingType: UNKNOWN_DIRECTIVE,
				Description: fmt.Sprintf(`Directive "%s" is not a known CSP directive.`, directive),
				Severity:    SEV_SYNTAX,
				Directive:   directive,
			})
		}
	}
	return findings
}

/**
 * Checks if semicolons are missing in the csp.
 *
 * Example policy where this check would trigger (missing semicolon before
 * start of object-src):
 *  script-src foo.bar object-src 'none'
 *
 * @param parsedCsp A parsed csp.
 */
func CheckMissingSemicolon(parsedCSP CSP) []Finding {

	findings := make([]Finding, 0)

	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		directiveValues := pair.Value.([]string)

		if len(directiveValues) == 0 {
			continue
		}

		for _, value := range directiveValues {
			if IsDirective(value) {
				findings = append(findings, Finding{
					FindingType: MISSING_SEMICOLON,
					Description: fmt.Sprintf(`Did you forget the semicolon? '%s' seems to be a directive, not a value.`, value),
					Severity:    SEV_SYNTAX,
					Directive:   directive,
					Value:       value,
				})
			}
		}
	}

	return findings
}

var VALID_SHA_PATTERN = regexp.MustCompile(`^(sha256|sha384|sha512)-`)

/**
 * Checks if csp contains invalid keywords.
 *
 * Example policy where this check would trigger:
 *  script-src 'notAkeyword'
 *
 * @param parsedCsp A parsed csp.
 */
func CheckInvalidKeyword(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	keywordNoTicks := make([]string, 0)
	for kw := range allKeywords {
		keywordNoTicks = append(keywordNoTicks, strings.ReplaceAll(kw.String(), "'", ""))
	}

	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		directiveValues := pair.Value.([]string)

		if len(directiveValues) == 0 {
			continue
		}

		for _, value := range directiveValues {
			if (Includes(keywordNoTicks, value)) || strings.HasPrefix(value, "nonce-") || VALID_SHA_PATTERN.MatchString(value) {
				findings = append(findings, Finding{
					FindingType: INVALID_KEYWORD,
					Description: fmt.Sprintf(`Did you forget to surround '%s' with single-ticks.`, value),
					Severity:    SEV_SYNTAX,
					Directive:   directive,
					Value:       value,
				})
				continue
			}

			// Continue, if the value doesn't start with single tick.
			// All CSP keywords start with a single tick.
			if !strings.HasPrefix(value, "'") {
				continue
			}

			if directive == REQUIRE_TRUSTED_TYPES_FOR {
				// Continue, if it's an allowed Trusted Types sink.
				if value == string(SCRIPT) {
					continue
				}
			} else if directive == TRUSTED_TYPES {
				// Continue, if it's an allowed Trusted Types keyword.
				if value == "'allow-duplicates'" || value == "'none'" {
					continue
				}
			} else {
				// Continue, if it's a valid keyword.
				if IsKeyword(value) || IsHash(value, false) || IsNonce(value, false) {
					continue
				}
			}

			findings = append(findings, Finding{
				FindingType: INVALID_KEYWORD,
				Description: fmt.Sprintf(`%s seems to be an invalid CSP keyword.`, value),
				Severity:    SEV_SYNTAX,
				Directive:   directive,
				Value:       value,
			})
		}
	}
	return findings
}
