package gocspevaluator

import (
	"fmt"
	"net"
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/vulnerability-research/foss/go-csp-evaluator/bypasses"
)

var DIRECTIVES_CAUSING_XSS = []Directive{SCRIPT_SRC, OBJECT_SRC, BASE_URI}

var URL_SCHEMES_CAUSING_XSS = []string{"data:", "http:", "https:"}

/**
 * Checks if passed csp allows inline scripts.
 * Findings of this check are critical and FP free.
 * unsafe-inline is ignored in the presence of a nonce or a hash. This check
 * does not account for this and therefore the effectiveCsp needs to be passed.
 *
 * Example policy where this check would trigger:
 *  script-src 'unsafe-inline'
 *
 * @param effectiveCsp A parsed csp that only contains values which
 *  are active in a certain version of CSP (e.g. no unsafe-inline if a nonce
 *  is present).
 */
func CheckScriptUnsafeInline(effectiveCsp CSP) []Finding {
	findings := make([]Finding, 0)

	directiveName := effectiveCsp.EffectiveDirective(SCRIPT_SRC)
	values := effectiveCsp.GetDirective(directiveName)

	if Includes(values, KW_UNSAFE_INLINE.String()) {

		findings = append(findings, Finding{
			FindingType: SCRIPT_UNSAFE_INLINE,
			Description: `'unsafe-inline' allows the execution of unsafe in-page scripts and event handlers.`,
			Severity:    SEV_HIGH,
			Directive:   directiveName,
			Value:       KW_UNSAFE_INLINE.String(),
		})
	}

	return findings
}

/**
 * Checks if passed csp allows eval in scripts.
 * Findings of this check have a medium severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  script-src 'unsafe-eval'
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckScriptUnsafeEval(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directiveName := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	values := parsedCSP.GetDirective(directiveName)

	// Check if unsafe-eval is present.
	if Includes(values, KW_UNSAFE_EVAL.String()) {

		findings = append(findings, Finding{
			FindingType: SCRIPT_UNSAFE_EVAL,
			Description: `'unsafe-eval' allows the execution of code injected into DOM APIs such as eval().`,
			Severity:    SEV_MEDIUM_MAYBE,
			Directive:   directiveName,
			Value:       KW_UNSAFE_EVAL.String(),
		})
	}

	return findings
}

/**
 * Checks if plain URL schemes (e.g. http:) are allowed in sensitive directives.
 * Findings of this check have a high severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  script-src https: http: data:
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckPlainUrlSchemes(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directivesToCheck := parsedCSP.EffectiveDirectives(DIRECTIVES_CAUSING_XSS)

	for _, directive := range directivesToCheck {

		values := parsedCSP.GetDirective(directive)
		for _, value := range values {

			if Includes(URL_SCHEMES_CAUSING_XSS, value) {
				findings = append(findings, Finding{
					FindingType: PLAIN_URL_SCHEMES,
					Description: fmt.Sprintf(`%s URI in %s allows the execution of unsafe scripts.`, value, directive),
					Severity:    SEV_HIGH,
					Directive:   directive,
					Value:       value,
				})
			}
		}
	}

	return findings
}

/**
 * Checks if csp contains wildcards in sensitive directives.
 * Findings of this check have a high severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  script-src *
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckWildcards(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directivesToCheck := parsedCSP.EffectiveDirectives(DIRECTIVES_CAUSING_XSS)

	for _, directive := range directivesToCheck {

		values := parsedCSP.GetDirective(directive)
		for _, value := range values {

			url := getSchemeFreeUrl(value)

			if strings.EqualFold(url, "*") {
				findings = append(findings, Finding{
					FindingType: PLAIN_WILDCARD,
					Description: fmt.Sprintf(`%s should not allow '*' as source`, directive),
					Severity:    SEV_HIGH,
					Directive:   directive,
					Value:       value,
				})
			}
		}
	}

	return findings
}

/**
 * Checks if object-src is restricted to none either directly or via a
 * default-src.
 */
func CheckMissingObjectSrcDirective(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	objectRestrictions := make([]string, 0)
	objectDirectives := parsedCSP.GetDirective(OBJECT_SRC)
	objectDefaultDirectives := parsedCSP.GetDirective(DEFAULT_SRC)

	if objectDirectives != nil {
		objectRestrictions = objectDirectives
	} else if objectDefaultDirectives != nil {
		objectRestrictions = objectDefaultDirectives
	}

	if len(objectRestrictions) >= 1 {
		return findings
	}

	findings = append(findings, Finding{
		FindingType: MISSING_DIRECTIVES,
		Description: `Missing object-src allows the injection of plugins which can execute JavaScript. Can you set it to 'none'?`,
		Severity:    SEV_HIGH,
		Directive:   OBJECT_SRC,
	})

	return findings
}

/**
 * Checks if script-src is restricted either directly or via a default-src.
 */
func CheckMissingScriptSrcDirective(parsedCSP CSP) []Finding {

	findings := make([]Finding, 0)

	_, scriptDirectivesExist := parsedCSP.directives.Get(SCRIPT_SRC)
	_, defaultDirectivesExist := parsedCSP.directives.Get(DEFAULT_SRC)

	if scriptDirectivesExist || defaultDirectivesExist {
		return findings
	}

	findings = append(findings, Finding{
		FindingType: MISSING_DIRECTIVES,
		Description: `script-src directive is missing.`,
		Severity:    SEV_HIGH,
		Directive:   SCRIPT_SRC,
	})

	return findings
}

/**
* Checks if the base-uri needs to be restricted and if so, whether it has been
* restricted.
 */
func CheckMissingBaseUriDirective(parsedCSP CSP) []Finding {
	return CheckMultipleMissingBaseUriDirective([]CSP{parsedCSP})
}

/**
* Checks if the base-uri needs to be restricted and if so, whether it has been
* restricted.
 */
func CheckMultipleMissingBaseUriDirective(parsedCSPs []CSP) []Finding {
	// base-uri can be used to bypass nonce based CSPs and hash based CSPs that
	// use strict dynamic

	findings := make([]Finding, 0)

	needsBaseURI := false
	hasBaseURI := false

	for _, csp := range parsedCSPs {
		if csp.PolicyHasScriptNonces() || (csp.PolicyHasScriptHashes() && csp.PolicyHasStrictDynamic()) {
			needsBaseURI = true
		}

		if exist := csp.GetDirective(BASE_URI); exist != nil {
			hasBaseURI = true
		}
	}

	if needsBaseURI && !hasBaseURI {
		findings = append(findings, Finding{
			FindingType: MISSING_DIRECTIVES,
			Description: `'Missing base-uri allows the injection of base tags.
			They can be used to set the base URL for all relative (script)
			URLs to an attacker controlled domain.
			Can you set it to 'none' or 'self'?;`,
			Severity:  SEV_HIGH,
			Directive: BASE_URI,
		})
	}

	return findings
}

/**
 * Checks if all necessary directives for preventing XSS are set.
 * Findings of this check have a high severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  script-src 'none'
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckMissingDirectives(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)
	findings = append(findings, CheckMissingObjectSrcDirective(parsedCSP)...)
	findings = append(findings, CheckMissingScriptSrcDirective(parsedCSP)...)
	findings = append(findings, CheckMissingBaseUriDirective(parsedCSP)...)
	return findings
}

/**
 * Checks if allowlisted origins are bypassable by JSONP/Angular endpoints.
 * High severity findings of this check are FP free.
 *
 * Example policy where this check would trigger:
 *  default-src 'none'; script-src www.google.com
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckScriptAllowlistBypass(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	effectiveScriptSrcDirective := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	scriptSrcValues := parsedCSP.GetDirective(effectiveScriptSrcDirective)

	if Includes(scriptSrcValues, KW_NONE.String()) {
		return findings
	}

	for _, value := range scriptSrcValues {
		if strings.EqualFold(value, KW_SELF.String()) {
			findings = append(findings, Finding{
				FindingType: SCRIPT_ALLOWLIST_BYPASS,
				Description: `'self' can be problematic if you host JSONP, AngularJS or user uploaded files.`,
				Severity:    SEV_MEDIUM_MAYBE,
				Directive:   effectiveScriptSrcDirective,
				Value:       value,
			})
			continue
		}

		// Ignore keywords, nonces and hashes (they start with a single quote).
		if strings.HasPrefix(value, "'") {
			continue
		}

		// Ignore standalone schemes and things that don't look like URLs (no dot).
		if IsUrlScheme(value) || !strings.Contains(value, ".") {
			continue
		}

		inURL := "//" + getSchemeFreeUrl(value)

		angularBypass, _ := matchWildcardURLs(inURL, bypasses.AngularURLS)

		jsonpBypass, _ := matchWildcardURLs(inURL, bypasses.JSONPURLs)

		// Some JSONP bypasses only work in presence of unsafe-eval.
		if jsonpBypass != nil {
			evalRequired := Includes(bypasses.NeedsEval, jsonpBypass.Hostname())
			evalPresent := Includes(scriptSrcValues, KW_UNSAFE_EVAL.String())
			if evalRequired && !evalPresent {
				jsonpBypass = nil
			}
		}

		if jsonpBypass != nil || angularBypass != nil {
			bypassDomain := ""
			bypassTxt := ""
			if jsonpBypass != nil {
				bypassDomain = jsonpBypass.Hostname()
				bypassTxt = " JSONP endpoints"
			}

			if angularBypass != nil {
				bypassDomain = angularBypass.Hostname()
				if strings.TrimSpace(bypassTxt) == "" {
					bypassTxt += ""
				} else {
					bypassTxt += " and"
				}
				bypassTxt += " Angular libraries"
			}
			findings = append(findings, Finding{
				FindingType: SCRIPT_ALLOWLIST_BYPASS,
				Description: fmt.Sprintf(`%s is known to host%s which allow to bypass this CSP.`, bypassDomain, bypassTxt), // space after 'host' purposely removed
				Severity:    SEV_HIGH,
				Directive:   effectiveScriptSrcDirective,
				Value:       value,
			})
		} else {
			findings = append(findings, Finding{
				FindingType: SCRIPT_ALLOWLIST_BYPASS,
				Description: `No bypass found; make sure that this URL doesn't serve JSONP replies or Angular libraries.`,
				Severity:    SEV_MEDIUM_MAYBE,
				Directive:   effectiveScriptSrcDirective,
				Value:       value,
			})
		}
	}

	return findings
}

/**
 * Checks if allowlisted object-src origins are bypassable.
 * Findings of this check have a high severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  default-src 'none'; object-src ajax.googleapis.com
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckFlashObjectAllowlistBypass(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	effectiveObjectSrcDirective := parsedCSP.EffectiveDirective(OBJECT_SRC)
	objectSrcValues := parsedCSP.GetDirective(effectiveObjectSrcDirective)

	// If flash is not allowed in plugin-types, continue.
	pluginTypes := parsedCSP.GetDirective(PLUGIN_TYPES)
	if pluginTypes != nil && !Includes(pluginTypes, "application/x-shockwave-flash") {
		return findings
	}

	for _, value := range objectSrcValues {
		// Nothing to do here if 'none'.
		if strings.EqualFold(value, KW_NONE.String()) {
			return findings
		}

		inURL := "//" + getSchemeFreeUrl(value)
		flashBypass, _ := matchWildcardURLs(inURL, bypasses.FlashURLS)

		if flashBypass != nil {
			findings = append(findings, Finding{
				FindingType: OBJECT_ALLOWLIST_BYPASS,
				Description: fmt.Sprintf(`%s is known to host Flash files which allow to bypass this CSP.`, flashBypass.Hostname()),
				Severity:    SEV_HIGH,
				Directive:   effectiveObjectSrcDirective,
				Value:       value,
			})
		} else if effectiveObjectSrcDirective == OBJECT_SRC {
			findings = append(findings, Finding{
				FindingType: OBJECT_ALLOWLIST_BYPASS,
				Description: `Can you restrict object-src to 'none' only?`,
				Severity:    SEV_MEDIUM_MAYBE,
				Directive:   effectiveObjectSrcDirective,
				Value:       value,
			})
		}
	}

	return findings
}

/**
 * Checks if csp contains IP addresses.
 * Findings of this check are informal only and are FP free.
 *
 * Example policy where this check would trigger:
 *  script-src 127.0.0.1
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckIPSource(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	// Apply check to values of all directives.
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		directiveValues := pair.Value.([]string)

		for _, value := range directiveValues {
			host := getHostname(value)

			if LooksLikeIPAddress(host) {
				// Check if localhost.
				// See 4.8 in https://www.w3.org/TR/CSP2/#match-source-expression
				if host == "127.0.0.1" {
					findings = append(findings, Finding{
						FindingType: IP_SOURCE,
						Description: fmt.Sprintf(`%s directive allows localhost as source. Please make sure to remove this in production environments`, directive),
						Severity:    SEV_INFO,
						Directive:   directive,
						Value:       value,
					})
				} else {
					findings = append(findings, Finding{
						FindingType: IP_SOURCE,
						Description: fmt.Sprintf(`%s directive has an IP-Address as source: %s (will be ignored by browsers!).`, directive, host),
						Severity:    SEV_INFO,
						Directive:   directive,
						Value:       value,
					})
				}
			}
		}
	}

	return findings
}

/**
 * Checks if csp contains directives that are deprecated in CSP3.
 * Findings of this check are informal only and are FP free.
 *
 * Example policy where this check would trigger:
 *  report-uri foo.bar/csp
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckDeprecatedDirective(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	// More details: https://www.chromestatus.com/feature/5769374145183744
	if _, exist := parsedCSP.directives.Get(REFLECTED_XSS); exist {
		findings = append(findings, Finding{
			FindingType: DEPRECATED_DIRECTIVE,
			Description: `reflected-xss is deprecated since CSP2. Please, use the X-XSS-Protection header instead.`,
			Severity:    SEV_INFO,
			Directive:   REFLECTED_XSS,
		})
	}

	// More details: https://www.chromestatus.com/feature/5680800376815616
	if _, exist := parsedCSP.directives.Get(REFERRER); exist {
		findings = append(findings, Finding{
			FindingType: DEPRECATED_DIRECTIVE,
			Description: `referrer is deprecated since CSP2. Please, use the Referrer-Policy header instead.`,
			Severity:    SEV_INFO,
			Directive:   REFERRER,
		})
	}

	// More details: https://github.com/w3c/webappsec-csp/pull/327
	if _, exist := parsedCSP.directives.Get(DISOWN_OPENER); exist {
		findings = append(findings, Finding{
			FindingType: DEPRECATED_DIRECTIVE,
			Description: `disown-opener is deprecated since CSP3. Please, use the Cross Origin Opener Policy header instead.`,
			Severity:    SEV_INFO,
			Directive:   DISOWN_OPENER,
		})
	}

	return findings
}

/**
 * Checks if csp nonce is at least 8 characters long.
 * Findings of this check are of medium severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  script-src 'nonce-short'
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckNonceLength(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	// Apply check to values of all directives.
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		directiveValues := pair.Value.([]string)

		for _, value := range directiveValues {

			matches := NONCE_PATTERN.FindAllStringSubmatch(value, -1)
			if len(matches) == 0 {
				continue
			}

			// Not a nonce
			nonceValue := matches[0][1]
			if len(nonceValue) < 8 {
				findings = append(findings, Finding{
					FindingType: NONCE_LENGTH,
					Description: `Nonces should be at least 8 characters long.`,
					Severity:    SEV_MEDIUM,
					Directive:   directive,
					Value:       value,
				})
			}

			if !IsNonce(value, true) {
				findings = append(findings, Finding{
					FindingType: NONCE_CHARSET,
					Description: `Nonces should only use the base64 charset.`,
					Severity:    SEV_INFO,
					Directive:   directive,
					Value:       value,
				})
			}
		}
	}

	return findings
}

/**
 * Checks if CSP allows sourcing from http://
 * Findings of this check are of medium severity and are FP free.
 *
 * Example policy where this check would trigger:
 *  report-uri http://foo.bar/csp
 *
 * @param parsedCsp Parsed CSP.
 */
func CheckSrcHttp(parsedCSP CSP) []Finding {

	findings := make([]Finding, 0)

	// Apply check to values of all directives.
	for pair := parsedCSP.directives.Oldest(); pair != nil; pair = pair.Next() {
		directive := pair.Key.(Directive)
		directiveValues := pair.Value.([]string)

		for _, value := range directiveValues {

			description := "Allow only resources downloaded over HTTPS."
			if directive == REPORT_URI {
				description = "Use HTTPS to send violation reports securely."
			}

			if strings.HasPrefix(value, "http://") {
				findings = append(findings, Finding{
					FindingType: SRC_HTTP,
					Description: description,
					Severity:    SEV_MEDIUM,
					Directive:   directive,
					Value:       value,
				})
			}
		}
	}

	return findings
}

/**
 * Checks if the policy has configured reporting in a robust manner.
 */
func CheckHasConfiguredReporting(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	reportURIValues := parsedCSP.GetDirective(REPORT_URI)
	if len(reportURIValues) > 0 {
		return findings
	}

	reportToValues := parsedCSP.GetDirective(REPORT_TO)
	if len(reportToValues) > 0 {
		findings = append(findings, Finding{
			FindingType: REPORT_TO_ONLY,
			Description: `This CSP policy only provides a reporting destination via the 'report-to' directive. This directive is only supported in Chromium-based browsers so it is recommended to also use a 'report-uri' directive.`,
			Severity:    SEV_INFO,
			Directive:   REPORT_TO,
		})
		return findings
	}

	findings = append(findings, Finding{
		FindingType: REPORTING_DESTINATION_MISSING,
		Description: `This CSP policy does not configure a reporting destination. This makes it difficult to maintain the CSP policy over time and monitor for any breakages.`,
		Severity:    SEV_INFO,
		Directive:   REPORT_URI,
	})

	return findings
}

/**
 * Returns whether the given string "looks" like an IP address. This function
 * only uses basic heuristics and does not accept all valid IPs nor reject all
 * invalid IPs.
 */
func LooksLikeIPAddress(maybeIP string) bool {
	if strings.HasPrefix(maybeIP, "[") && strings.HasSuffix(maybeIP, "]") {
		val := maybeIP[1 : len(maybeIP)-1]
		return net.ParseIP(val) != nil
	}
	return net.ParseIP(maybeIP) != nil
}

var REMOVE_URL_PATTERN = regexp.MustCompile(`^\w[+\w.-]*:\/\/`)

var REMOVE_PROTO_AGNOSTIC = regexp.MustCompile(`^\/\/`)

/**
 * Removes scheme from url.
 * @param url Url.
 * @return url without scheme.
 */
func getSchemeFreeUrl(inURL string) string {
	// Remove URL scheme.
	inURL = REMOVE_URL_PATTERN.ReplaceAllString(inURL, "")
	// Remove protocol agnostic "//"
	inURL = REMOVE_PROTO_AGNOSTIC.ReplaceAllString(inURL, "")
	return inURL
}

func setScheme(inURL string) string {
	if strings.HasPrefix(inURL, "//") {
		return strings.Replace(inURL, "//", "https://", 1)
	}
	return inURL
}

var WILDCARD_PLACEHOLDER = regexp.MustCompile("^wildcard_placeholder")

func matchWildcardURLs(cspURLString string, listOfURLStrings []string) (*url.URL, error) {
	removedWildcardPort := strings.ReplaceAll(cspURLString, ":*", "") // Remove wildcard port
	replaced := strings.ReplaceAll(removedWildcardPort, "*", "wildcard_placeholder")
	// non-Chromium browsers don't support wildcards in domain names. We work
	// around this by replacing the wildcard with `wildcard_placeholder` before
	// parsing the domain and using that as a magic string. This magic string is
	// encapsulated in this function such that callers of this function do not
	// have to worry about this detail.
	cspURL, err := url.Parse(setScheme(replaced))
	if err != nil {
		return nil, err
	}

	listOfURLs := make([]*url.URL, 0)
	for _, inURL := range listOfURLStrings {
		l, err := url.Parse(setScheme(inURL))
		if err != nil {
			return nil, err
		}

		listOfURLs = append(listOfURLs, l)
	}

	host := strings.ToLower(cspURL.Hostname())
	hostHasWildcard := strings.HasPrefix(host, "wildcard_placeholder.")
	wildcardFreeHost := WILDCARD_PLACEHOLDER.ReplaceAllString(host, "")
	path := cspURL.Path
	hasPath := path != "" // translation is different js URL returns "/" Go returns ""

	for _, listURL := range listOfURLs {
		domain := listURL.Hostname()
		if !strings.HasSuffix(domain, wildcardFreeHost) {
			// Domains do not match
			continue
		}

		// If the host has no subdomain wildcard and doesn't match, continue.
		if !hostHasWildcard && !strings.EqualFold(host, domain) {
			continue
		}

		// If the allowlisted url has a path, check if one of the url paths
		// match.
		if hasPath {
			if strings.HasSuffix(path, "/") {
				if !strings.HasPrefix(listURL.Path, path) {
					continue
				}
			} else {
				if listURL.Path != path {
					// Path doesn't match
					continue
				}
			}
		}
		// We found a match.
		return listURL, nil
	}

	// No match was found.
	return nil, nil
}

var IPV6_PATTERN = regexp.MustCompile(`^\[[\d:]+\]`)

func getHostname(inURL string) string {
	removedWildcardPort := strings.ReplaceAll(getSchemeFreeUrl(inURL), ":*", "") // Remove wildcard port
	hostname, err := url.Parse("https://" + strings.ReplaceAll(removedWildcardPort, "*", "wildcard_placeholder"))
	if err != nil {
		return ""
	}

	host := strings.ReplaceAll(hostname.Hostname(), "wildcard_placeholder", "*")
	// Some browsers strip the brackets from IPv6 addresses when you access the
	// hostname. If the scheme free url starts with something that vaguely looks
	// like an IPv6 address and our parsed hostname doesn't have the brackets,
	// then we add them back to work around this
	if IPV6_PATTERN.MatchString(getSchemeFreeUrl(host)) && !IPV6_PATTERN.MatchString(host) {
		return "[" + host + "]"
	}
	return host
}
