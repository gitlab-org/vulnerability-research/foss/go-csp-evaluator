package gocspevaluator

import (
	"strings"
)

func CheckStrictDynamic(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directiveName := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	values := parsedCSP.GetDirective(directiveName)

	schemeOrHostPresent := false
	for _, value := range values {
		if !strings.HasPrefix(value, "'") {
			schemeOrHostPresent = true
			break
		}
	}

	if schemeOrHostPresent && !Includes(values, KW_STRICT_DYNAMIC.String()) {
		findings = append(findings, Finding{
			FindingType: STRICT_DYNAMIC,
			Description: `Host allowlists can frequently be bypassed. Consider using 'strict-dynamic' in combination with CSP nonces or hashes.`,
			Severity:    SEV_STRICT_CSP,
			Directive:   directiveName,
		})
	}

	return findings
}

/**
 * Checks if 'strict-dynamic' is only used together with a nonce or a hash.
 *
 * Example policy where this check would trigger:
 *  script-src 'strict-dynamic'
 *
 * @param parsedCsp A parsed csp.
 */
func CheckStrictDynamicNotStandalone(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directiveName := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	values := parsedCSP.GetDirective(directiveName)

	if Includes(values, KW_STRICT_DYNAMIC.String()) &&
		(!parsedCSP.PolicyHasScriptNonces() && !parsedCSP.PolicyHasScriptHashes()) {

		findings = append(findings, Finding{
			FindingType: STRICT_DYNAMIC_NOT_STANDALONE,
			Description: `'strict-dynamic' without a CSP nonce/hash will block all scripts.`,
			Severity:    SEV_INFO,
			Directive:   directiveName,
		})
	}

	return findings
}

/**
 * Checks if the policy has 'unsafe-inline' when a nonce or hash are present.
 * This will ensure backward compatibility to browser that don't support
 * CSP nonces or hasehs.
 *
 * Example policy where this check would trigger:
 *  script-src 'nonce-test'
 *
 * @param parsedCsp A parsed csp.
 */
func CheckUnsafeInlineFallback(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)
	if !parsedCSP.PolicyHasScriptNonces() && !parsedCSP.PolicyHasScriptHashes() {
		return findings
	}

	directiveName := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	values := parsedCSP.GetDirective(directiveName)

	if !Includes(values, KW_UNSAFE_INLINE.String()) {

		findings = append(findings, Finding{
			FindingType: UNSAFE_INLINE_FALLBACK,
			Description: `Consider adding 'unsafe-inline' (ignored by browsers supporting nonces/hashes) to be backward compatible with older browsers.`,
			Severity:    SEV_STRICT_CSP,
			Directive:   directiveName,
		})
	}

	return findings
}

/**
 * Checks if the policy has an allowlist fallback (* or http: and https:) when
 * 'strict-dynamic' is present.
 * This will ensure backward compatibility to browser that don't support
 * 'strict-dynamic'.
 *
 * Example policy where this check would trigger:
 *  script-src 'nonce-test' 'strict-dynamic'
 *
 * @param parsedCsp A parsed csp.
 */
func CheckAllowlistFallback(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directiveName := parsedCSP.EffectiveDirective(SCRIPT_SRC)
	values := parsedCSP.GetDirective(directiveName)

	if !Includes(values, KW_STRICT_DYNAMIC.String()) {
		return findings
	}

	// Check if there's already an allowlist (url scheme or url)
	hasAllowList := true
	for _, value := range values {
		// we are just doing equality checks for first 3 comparisons as that is how you'd translate:
		// ['http:', 'https:', '*'].includes(value)
		if value == "http:" || value == "https:" || value == "*" || strings.Contains(value, ".") {
			hasAllowList = false
			break
		}
	}

	if hasAllowList {

		findings = append(findings, Finding{
			FindingType: ALLOWLIST_FALLBACK,
			Description: `Consider adding https: and http: url schemes (ignored by browsers supporting 'strict-dynamic') to be backward compatible with older browsers.`,
			Severity:    SEV_STRICT_CSP,
			Directive:   directiveName,
		})
	}

	return findings
}

/**
 * Checks if the policy requires Trusted Types for scripts.
 *
 * I.e. the policy should have the following dirctive:
 *  require-trusted-types-for 'script'
 *
 * @param parsedCsp A parsed csp.
 */
func CheckRequiresTrustedTypesForScripts(parsedCSP CSP) []Finding {
	findings := make([]Finding, 0)

	directiveName := parsedCSP.EffectiveDirective(REQUIRE_TRUSTED_TYPES_FOR)
	values := parsedCSP.GetDirective(directiveName)

	if !Includes(values, string(SCRIPT)) {
		findings = append(findings, Finding{
			FindingType: REQUIRE_TRUSTED_TYPES_FOR_SCRIPTS,
			Description: `Consider requiring Trusted Types for scripts to lock down DOM XSS injection sinks. You can do this by adding '"require-trusted-types-for 'script'" to your policy.`,
			Severity:    SEV_INFO,
			Directive:   REQUIRE_TRUSTED_TYPES_FOR,
		})
	}

	return findings
}
