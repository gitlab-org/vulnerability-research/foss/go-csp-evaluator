package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckScriptDynamic(t *testing.T) {
	policy := "script-src foo.bar"
	findings := testCheckCSP(policy, CheckStrictDynamic)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_STRICT_CSP, findings[0].Severity)
}

func TestCheckStrictDynamicNotStandalone(t *testing.T) {
	policy := "script-src 'strict-dynamic'"
	findings := testCheckCSP(policy, CheckStrictDynamicNotStandalone)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_INFO, findings[0].Severity)
}

func TestCheckStrictDynamicNotStandaloneDoesntFireIfNoncePresent(t *testing.T) {
	policy := "script-src 'strict-dynamic' 'nonce-foobar'"
	findings := testCheckCSP(policy, CheckStrictDynamicNotStandalone)
	assert.Len(t, findings, 0)
}

func TestCheckUnsafeInlineFallback(t *testing.T) {
	policy := "script-src 'nonce-test'"
	findings := testCheckCSP(policy, CheckUnsafeInlineFallback)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_STRICT_CSP, findings[0].Severity)
}

func TestCheckAllowlistFallback(t *testing.T) {
	policy := "script-src 'nonce-test' 'strict-dynamic'"
	findings := testCheckCSP(policy, CheckAllowlistFallback)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_STRICT_CSP, findings[0].Severity)
}

func TestCheckAllowlistFallbackDoesntFireIfSchemeFallbackPresent(t *testing.T) {
	policy := "script-src 'nonce-test' 'strict-dynamic' https:"
	findings := testCheckCSP(policy, CheckAllowlistFallback)
	assert.Len(t, findings, 0)
}

func TestCheckAllowlistFallbackDoesntFireIfURLFallbackPresent(t *testing.T) {
	policy := "script-src 'nonce-test' 'strict-dynamic' foo.bar"
	findings := testCheckCSP(policy, CheckAllowlistFallback)
	assert.Len(t, findings, 0)
}

func TestCheckAllowlistFallbackDoesntFireInAbsenceOfStrictDynamic(t *testing.T) {
	policy := "script-src 'nonce-test'"
	findings := testCheckCSP(policy, CheckAllowlistFallback)
	assert.Len(t, findings, 0)
}

func TestCheckRequiresTrustedTypesForScripts(t *testing.T) {
	policy := "require-trusted-types-for 'script'"
	findings := testCheckCSP(policy, CheckRequiresTrustedTypesForScripts)
	assert.Len(t, findings, 0)

	policy = "unknown 'script'"
	findings = testCheckCSP(policy, CheckRequiresTrustedTypesForScripts)
	assert.Len(t, findings, 1)

	policy = "require-trusted-types-for 'unknown'"
	findings = testCheckCSP(policy, CheckRequiresTrustedTypesForScripts)
	assert.Len(t, findings, 1)
}
